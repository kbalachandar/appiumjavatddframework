package MFMA.Factories;


import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;

import MFMA.Screens.WorkflowsScreen;
import genericLibrary.Log;
import genericLibrary.MobileDriverUtils;
import genericLibrary.StopWatch;
import genericLibrary.Utils;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class WorkflowsFactory extends WorkflowsScreen {

	RemoteWebDriver driver;
	
	/**
	 * WorkflowOperations : WorkflowOperations constructor
	 * @param driver : RemoteWebdriver
	 * @throws Exception 
	 */
	public WorkflowsFactory(RemoteWebDriver driver) throws Exception {
		
		final long startTime = StopWatch.startTime();
		
		try {
			this.driver = driver;
			PageFactory.initElements(new AppiumFieldDecorator(driver), this);
        }
        catch (Exception e) {
        	throw new Exception("Exception at WorkflowOperations constructor : " + e);
        }
        finally {
        	Log.event("WorkflowOperations Constructor", StopWatch.elapsedTime(startTime));
        }
    } //End WorkflowOperations
	
	final public void isLoaded(){
    	
	}//isLoaded
	
	final protected void load(){
		
	}//load

	/**
	 * setWorkflow : Set Workflow
	 * @param Null
	 * @throws Exception 
	 */
    public void setWorkflow(String workflowName) throws Exception
    {
    	int count = 0;
    	final long startTime = StopWatch.startTime();
		
		try	
		{
			Thread.sleep(5000);
			Utils.waitForElement(driver, spinnerWorkflowName);
		
			if(spinnerWorkflowName.isDisplayed() && workflowName != null)  	{
				spinnerWorkflowName.click();
    		
	    		if(spinnerTitle.isDisplayed())
	    		{
	    			if(MobileDriverUtils.platform.equalsIgnoreCase("Android"))
	    			{
	    				int listCount = optionsText.size();
		    			
		    			while(count < listCount){		
		    	    		if(optionsText.get(count).getText().contentEquals(workflowName)){
		    	    			optionsText.get(count).click();
		    	    			break;
		    	    		} //End If
		    	    		count++;
		    	    	} //End While
	    			}
	    			else
	    			{
	    				driver.findElement(By.xpath("//UIATableCell[@name='" + workflowName + "']")).click();
	    			}
				}//End If
	    	}//End If
		}
		catch (Exception e) {
			throw new Exception("Exception at WorkflowsPage setWorkflow : " + e);
		}
    
		finally	{
			Thread.sleep(5000);
			Log.event("WorkflowsPage setWorkflow", StopWatch.elapsedTime(startTime));
		}
    
    } //End setWorkflow
    
    /**
	 * setWorkflowState : Set Workflow State
	 * @param Null
	 * @throws Exception 
	 */
    public void setWorkflowState(String workflowStateName) throws Exception
    {
    	int count = 0;
    	final long startTime = StopWatch.startTime();
		
		try	{
	    	if(spinnerWorkflowValue.isDisplayed() && workflowStateName != null)  	{
	    		spinnerWorkflowValue.click();
	    		
	    		if(spinnerTitle.isDisplayed()){
	    			
	    			if(MobileDriverUtils.platform.equalsIgnoreCase("Android"))
	    			{
	    				int listCount = optionsText.size();
		    			
		    			while(count < listCount){		
		    	    		if(optionsText.get(count).getText().contentEquals(workflowStateName)){
		    	    			optionsText.get(count).click();
		    	    			break;
		    	    		} //End If
		    	    		count++;
		    	    	} //End While
	    			}
	    			else
	    			{
	    				driver.findElement(By.xpath("//UIATableCell[@name='" + workflowStateName + "']")).click();
	    			}
	    			
	    		}//End If
	    	}//End If
		}
    	catch (Exception e) {
        	throw new Exception("Exception at WorkflowsPage setWorkflowState : " + e);
	    }
	    
	    finally	{
	    	Thread.sleep(5000);
    		Log.event("WorkflowsPage setWorkflowState", StopWatch.elapsedTime(startTime));
    	}
    } //End setWorkflowState
    
    /**
	 * addComments : add comments
	 * @param Null
	 * @throws Exception 
	 */
    public void addComments(String comments) throws Exception
    {
    	final long startTime = StopWatch.startTime();
		
		try	{
	    	if(editorWorkflowComment.isEnabled() && comments != null)  	{
	    		editorWorkflowComment.sendKeys(comments);	
	    		}
		 	}
		catch (Exception e) {
	    	throw new Exception("Exception at WorkflowsPage addComments : " + e);
	    	}
	    
	    finally	{
	    	Thread.sleep(5000);
			Log.event("WorkflowsPage addComments", StopWatch.elapsedTime(startTime));
			}
    } //End addComments
        
    /**
	 * SaveWorkflow : Save Workflow
	 * @param Null
	 * @throws Exception 
	 */
    public void SaveWorkflow() throws Exception
    {
    	final long startTime = StopWatch.startTime();
		
		try	{
			if(btnSave.isDisplayed() && btnSave.isEnabled())  	{
				btnSave.click();	
			}
	    }
		catch (Exception e) {
	    	throw new Exception("Exception at WorkflowsPage SaveWorkflow : " + e);
	    	}
	    
	    finally	{
	    	Thread.sleep(5000);
			Log.event("WorkflowsPage SaveWorkflow", StopWatch.elapsedTime(startTime));
			}
    } //End SaveWorkflow
    
    /**
	 * cancelWorkflow : Cancel Workflow
	 * @param Null
	 * @throws Exception 
	 */
    public void cancelWorkflow() throws Exception
    {
    	if(Utils.waitForElement(driver, discardMessage))  	{
    		btnYes.click();	
    	}
    	
    } //End CancelWorkflow
    
    /**
	 * eSignSetUserName : enter UserName
	 * @param Null
	 * @throws Exception 
	 */
    public void eSignSetUserName(String userName) throws Exception
    {
    	if(editUserName.isEnabled() && userName != null)  	{
    		editUserName.sendKeys(userName);	
    	}
    	
    } //End enterUserName
    
    /**
	 * eSignEnterPassword : enter Password
	 * @param Null
	 * @throws Exception 
	 */
    public void eSignEnterPassword(String password) throws Exception
    {
    	final long startTime = StopWatch.startTime();
		
		try	{
			if(editUserPassword.isEnabled() && password != null)  	{
				editUserPassword.sendKeys(password);	
			}
	    }
		catch (Exception e) {
	    	throw new Exception("Exception at WorkflowsPage enterPassword : " + e);
	    	}
	    
	    finally	{
	    	Thread.sleep(5000);
			Log.event("WorkflowsPage enterPassword", StopWatch.elapsedTime(startTime));
			}
    } //End enterPassword
    
    /**
	 * Verify Error Message
	 * @param None
	 * @return Returns the error message if returns occurs.
	 * @throws Exception 
	 */
    public String verifyError() throws Exception {
    	
    	final long startTime = StopWatch.startTime();
    	
    	try {
    		
    		if (Utils.doesElementExist(driver, errorTitle))
    			return errorTitle.getText();  			
        	
    	}//End try
    	catch (Exception e) {
    		throw new Exception("Exception at WorkflowsPage.verifyError : " + e);
    	} //End catch
    	
    	finally {
    		Log.event("WorkflowsPage.verifyError.", StopWatch.elapsedTime(startTime));
    	} //End finally
    	
    	return " ";
    	
    } //verifyError
        
    /**
	 * Verify Error Message
	 * @param None
	 * @return Returns the error message if returns occurs.
	 * @throws Exception 
	 */
    public String getErrorInfo() throws Exception {
    	
    	final long startTime = StopWatch.startTime();
    	
    	try {
    		
    		//Utils.fluentWait(driver);
    		
    		if (Utils.doesElementExist(driver, errorTitle, 5))
    			return errorTitle.getText();  			
        	
    	}//End try
    	catch (Exception e) {
    		throw new Exception("Exception at WorkflowsPage.verifyError : " + e);
    	} //End catch
    	
    	finally {
    		Log.event("WorkflowsPage.verifyError.", StopWatch.elapsedTime(startTime));
    	} //End finally
    	
    	return " ";
    	
    } //verifyError
    
    /**
	 * eSignClickSave : Save Esign
	 * @param Null
	 * @throws Exception 
	 */
    public void eSignClickSave() throws Exception
    {
    	final long startTime = StopWatch.startTime();
		
		try	{
			if(btnSign.isDisplayed() && btnSign.isEnabled())  	{
    		btnSign.click();	
			}
	    }
		catch (Exception e) {
	    	throw new Exception("Exception at WorkflowsPage saveEsign : " + e);
	    	}
	    
	    finally	{
	    	Thread.sleep(5000);
			Log.event("WorkflowsPage saveEsign", StopWatch.elapsedTime(startTime));
			}
    } //End saveEsign
    
    /**
	 * cancelEsign : Cancel Esign
	 * @param Null
	 * @throws Exception 
	 */
    public void eSignClickCancel() throws Exception
    {
    	if(btnCancel.isEnabled())  	{
    		btnCancel.click();	
    	}
    	
    } //End cancelEsign
    
} //End WorkflowOperations
