package MFMA.Factories;


import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;

import MFMA.Screens.SearchScreen;
import genericLibrary.Log;
import genericLibrary.MobileDriverUtils;
import genericLibrary.StopWatch;
import genericLibrary.Utils;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
 
public class SearchFactory extends SearchScreen {
	
	RemoteWebDriver driver;
	
	/**
	 * SearchView : SearchView constructor
	 * @param driver : RemoteWebdriver
	 * @throws Exception 
	 */
	public SearchFactory(RemoteWebDriver driver) throws Exception {
		
		final long startTime = StopWatch.startTime();
		
		try {
			this.driver = driver;
			PageFactory.initElements(new AppiumFieldDecorator(driver), this);
        }
        catch (Exception e) {
        	throw new Exception("Exception at SearchView constructor : " + e);
        }
        finally {
        	Log.event("SearchView Constructor", StopWatch.elapsedTime(startTime));
        }
    } //End SearchView
	
	final public void isLoaded(){
    	
	}//isLoaded
	
	final protected void load(){
		
	}//load

	/**
	 * keywordSearch: This method is to perform simple search
	 * @param keyword
	 * @return None
	 * @throws Exception
	 */
	public void keywordSearch(String keyword) throws Exception {
    	
		final long startTime = StopWatch.startTime();
		
		try {
	    	Utils.waitForElement(driver, editorSearch);
	 
	    	if(MobileDriverUtils.platform.equalsIgnoreCase("Android"))
    		{
	    		if (!editorSearch.isEnabled() || keyword.equals(null))
		    		throw new Exception("Keyword search field is not enabled or keyword is null.");
		    	
		    	editorSearch.clear();
	    		editorSearch.sendKeys(keyword);
	    		Utils.pressKeyCode(driver, 66);
    		}
	    	else
	    	{
	    		editorSearch.click();
	    		editorSearch.clear();
	    		editorSearch.sendKeys(keyword);
	    		btnKeyboardSearch.click();
	    	}  		
		} // End try
		catch (Exception e) {
			throw new Exception("Exception at SearchPage.keywordSearch : ", e);
		} // End catch

		finally {
			Log.event("SearchPage.keywordSearch :", StopWatch.elapsedTime(startTime));
		}
		
    } //End keywordSearch
        
	/**
	 * vaultHome : select an object
	 * @param Null
	 * @throws Exception 
	 */
    public void selectObject(String objectName) throws Exception
    {
    	final long startTime = StopWatch.startTime();
		
		try {
				Utils.waitForElement(driver, objectListView);
			
				if(MobileDriverUtils.platform.equalsIgnoreCase("Android"))
	    		{
					int count = 0;
					int listCount = titleObjectsName.size();
					
					if(listCount == 0 && countGroupTitle.isDisplayed()){
						countGroupTitle.click();
						Utils.waitForElement(driver, titleObjectsName.get(0));
						listCount = titleObjectsName.size();
					}
			    	 
			    	while(count < listCount){		
			    		if(titleObjectsName.get(count).getText().contentEquals(objectName)){
			    			titleObjectsName.get(count).click();
			    			break;
			    		} //End If
			    		count++;
			    	} //End While
			    	
			    	if(count >= listCount && listCount !=0)	{
			    		Utils.swipe(driver, titleObjectsName.get(listCount-1), titleObjectsName.get(0), 3000);
			    		Utils.waitForElement(driver, objectListView);
			    		selectObject(objectName);
			    	}
	    		}
		    	else
		    	{
		    		driver.findElement(By.xpath("//UIATableCell[@name='" + objectName + "']")).click();
		    		Thread.sleep(5000);
		    	}
	    } // End try
		catch (Exception e) {
			throw new Exception("Exception at SearchPage.selectObject : ", e);
		} // End catch
	
		finally {
			Log.event("SearchPage.selectObject :", StopWatch.elapsedTime(startTime));
		}
    } //End selectObject
        
    /**
	 * Navigate to Object Creation Screen
	 * @return instance of ObjectCreation Screen
	 * @throws Exception 
	 */
    public ObjectCreationFactory navigateToObjectCreationScreen() throws Exception
    {
    	Log.event("Navigated to Object Creation Screen!");
		return (ObjectCreationFactory) new ObjectCreationFactory(driver).get();
    }

} //End class SearchView