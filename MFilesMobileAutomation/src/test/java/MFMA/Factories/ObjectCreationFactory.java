package MFMA.Factories;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import MFMA.Screens.ObjectCreationScreen;
import genericLibrary.Log;
import genericLibrary.MobileDriverUtils;
import genericLibrary.StopWatch;
import genericLibrary.Utils;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class ObjectCreationFactory extends ObjectCreationScreen 
{
	RemoteWebDriver driver;
	private boolean isPageLoaded = false;
	
	/**
	 * constructor of the class
	 * 
	 * @param driver
	 *            : RemoteWebdriver
	 */
	public ObjectCreationFactory(RemoteWebDriver driver){

    	this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }
	
	final public void isLoaded(){
		try {
			isPageLoaded = Utils.waitForElement(driver, btnAddFile);
		}
		catch (TimeoutException e) {
			throw e;
		}
	}//isLoaded
	
	final protected void load(){
		try {
			Utils.fluentWait(driver);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		isPageLoaded = true;
	}//load
	
	/**
	 * Verifying whether page loaded properly
	 * @return true if page loaded
	 */
	public Boolean pageLoaded()
	{
		isLoaded();
		return isPageLoaded;
	}
	
	/**
	 * Populating property values for the current object
	 * @Param objectProperties: HashMap with property Name and values
	 */
	@SuppressWarnings("rawtypes")
	public void populatingValuesWithoutType(LinkedHashMap <String, String> objectProperties) throws Exception
	{
		final long startTime = StopWatch.startTime();
		
		Set objectPropertiesSet = objectProperties.entrySet();
		Iterator objectPropertiesIterator = objectPropertiesSet.iterator();
		
		while (objectPropertiesIterator.hasNext()) {

			Map.Entry mapEntry = (Map.Entry) objectPropertiesIterator.next();
			String propertyKey = mapEntry.getKey().toString();
			String propertyValue = mapEntry.getValue().toString();
			this.setPropertyValue(propertyKey,propertyValue);
			
		}
	}
	
	/**
	 * Sets property value for a property
	 * @Param propertyName: Name of the property
	 * @Param propertyValue: Value of the property
	 */
	public void setPropertyValue(String propertyName, String propertyValue) throws Exception
	{
		WebElement text = null;
		WebElement list = null;
		WebElement date = null;
		int count,listCount;
		Boolean propertyMatch = false;
		Utils.waitForElement(driver, listProperties);
		if(MobileDriverUtils.platform.equalsIgnoreCase("Android"))
		{
			count = 0;
			listCount = txtPropertyKeys.size();
		}
		else
		{
			count = 2;
			listCount = txtProperties.size();
		}
    	while(count < listCount)
    	{
    		if(MobileDriverUtils.platform.equalsIgnoreCase("Android"))
    		{
    			if(txtPropertyKeys.get(count).getText().contains(propertyName))
        		{
        			//Boolean text = Utils.doesElementExist(driver, txtPropertyValues.get(count).findElement(By.className("android.widget.EditText")));
        			try
        			{
        				text = txtPropertyValues.get(count).findElement(By.className("android.widget.EditText"));
        				if(text!=null)
            			{
            				txtPropertyValues.get(count).click();
            				txtPropertyValues.get(count).sendKeys(propertyValue);
            				driver.navigate().back();
            				break;
            			}
        			}
        			catch(Exception e) { System.out.println(e);}	
        			try
        			{
        				list = txtPropertyValues.get(count).findElement(By.id("property_lookup_value_add_button"));
        				if(list!=null)
            			{
            				txtPropertyValues.get(count).findElement(By.id("property_lookup_value_add_button")).click();
            				this.selectListPropertyValue(propertyValue);
            				break;
            			}
        			}
        			catch(Exception e) { System.out.println(e);}	
        			try
        			{
        				date = txtPropertyValues.get(count).findElement(By.id("property_datetime_input"));
        				if(date!=null)
            			{
            				txtPropertyValues.get(count).click();
            				this.selectDatePropertyValue(propertyValue);
                			break;
            			}
        				else
            			{
            				Log.event("Not Matched with any property type");
            				break;
            			}
        			}
        			catch(Exception e) { System.out.println(e);}
        		} //End If
    		}
    		else
    		{
    			try
    			{
    				if(txtProperties.get(count).findElement(By.className("UIAStaticText")).getText().contains(propertyName.toUpperCase()))
            		{
            			try
            			{
            				text = txtProperties.get(count).findElement(By.className("UIATextView"));
            				if(text!=null)
                			{
            					txtProperties.get(count).findElement(By.className("UIATextView")).click();
            					if(Utils.doesElementExist(driver, btnProperty))
            					{
            						txtProperty.clear();
            						txtProperty.sendKeys(propertyValue);
            						btnProperty.click();
            					}
            					else
            					{
            						txtProperties.get(count).findElement(By.className("UIATextView")).clear();
            						txtProperties.get(count).findElement(By.className("UIATextView")).sendKeys(propertyValue + "\n");
            					}
            					
                				break;
                			}
            			}
            			catch(Exception e) { System.out.println(e);}	
            			try
            			{
            				list = txtProperties.get(count).findElement(By.className("UIATableView"));
            				if(list!=null)
                			{
            					txtProperties.get(count).findElement(By.className("UIATableView")).click();
                				this.selectListPropertyValue(propertyValue);
                				break;
                			}
            			}
            			catch(Exception e) { System.out.println(e);}	
            			try
            			{
            				date = txtProperties.get(count).findElement(By.className("UIAButton"));
            				if(date!=null)
                			{
            					txtProperties.get(count).findElement(By.className("UIAButton")).click();
                				this.selectDatePropertyValue(propertyValue);
                    			break;
                			}
            				else
                			{
                				Log.event("Not Matched with any property type");
                				break;
                			}
            			}
            			catch(Exception e) { System.out.println(e);}		
            		}
    			}
    			catch(Exception e) { System.out.println(e);}
        	} 
    		count++;
    	} //End While

    	if(count >= listCount)	
    	{
    		Utils.swipe(driver, txtPropertyKeys.get(listCount-1), txtPropertyKeys.get(0), 3000);
    		Utils.waitForElement(driver, listProperties);
    		setPropertyValue(propertyName,propertyValue);
    	}		
	}
	
	/**
	 * Sets list value for the list property type
	 * @Param propertyValue: Value of the property
	 */
	public void selectListPropertyValue(String propertyValue) throws Exception
    {
    	Utils.waitForElement(driver, listAddValueAlertValues);
		int count = 0;
		int listCount = txtAddValueAlertValues.size();
    	 
    	while(count < listCount){
    		if(MobileDriverUtils.platform.equalsIgnoreCase("Android"))
    		{
    			if(txtAddValueAlertValues.get(count).getText().contentEquals(propertyValue)){
	    			txtAddValueAlertValues.get(count).click();
	    			break;
    			}
    		}
    		else
    		{
    			if(txtAddValueAlertValues.get(count).findElement(By.className("UIAStaticText")).getText().contentEquals(propertyValue)){
	    			txtAddValueAlertValues.get(count).click();
	    			break;
    			}    		
    		} //End If
    		count++;
    	} //End While
    	
    	if(count >= listCount)	{
    		Log.event("Entered Swipe Block");
    		Utils.swipe(driver, txtAddValueAlertValues.get(listCount-1), txtAddValueAlertValues.get(0), 3000);
    		Utils.waitForElement(driver, listAddValueAlertValues);
    		selectListPropertyValue(propertyValue);
    	}
    } //End selectClassName
	
	/**
	 * Sets date value for the date property type
	 * @Param propertyValue: Value of the property
	 */
	public void selectDatePropertyValue(String dateValue) throws Exception
	{
		String[] dateValues = dateValue.split("_");
		
		if(MobileDriverUtils.platform.equalsIgnoreCase("Android"))
		{
			while(!dateNumberSelectedText.get(0).getText().equalsIgnoreCase(dateValues[0].substring(0, 3)))
			{
				Utils.swipe(driver, dateNumberSelectedText.get(0), dateNumberButtons.get(0), 1000);
			}
			
			while(!dateNumberSelectedText.get(1).getText().equalsIgnoreCase(dateValues[1]))
			{
				Utils.swipe(driver, dateNumberSelectedText.get(1), dateNumberButtons.get(2), 1000);
			}
			
			while(!dateNumberSelectedText.get(2).getText().equalsIgnoreCase(dateValues[2]))
			{
				Utils.swipe(driver, dateNumberSelectedText.get(2), dateNumberButtons.get(4), 1000);
			}
		}
		else
		{
			txtDatePropertyMonth.sendKeys(dateValues[0]);
			txtDatePropertyDay.sendKeys(dateValues[1]);
			txtDatePropertyYear.sendKeys(dateValues[2]);
		}
		btnSetDate.click();
	}
	
	public void tapOnSaveButton() throws Exception
	{
		btnSave.click();
	}
	
	public VaultHomeFactory tapOnBackButton() throws Exception
	{
		btnMenu.click();
		return new VaultHomeFactory(driver);
	}
	
	/**
     * cickMenuItem : Click on the given menu option
     * @param menu option to click 
     * @throws Exception 
     */

	public void clickMenuItem(String menuOption) throws Exception {
		Utils.waitForElement(driver, btnCMenu);
		final long startTime = StopWatch.startTime();
		
		try	{
			if(btnCMenu.isDisplayed())	{
				Thread.sleep(5000);
				btnCMenu.click();
				Thread.sleep(5000);
			}	
			if(MobileDriverUtils.platform.equalsIgnoreCase("Android"))
			{
				int count = 0;
				int listCount = txtMenuTitle.size();
			    	 
			    while(count < listCount){		
			    	if(txtMenuTitle.get(count).getText().equalsIgnoreCase(menuOption)){
			    		txtMenuTitle.get(count).click();
			    		break;
			    	} //End If   
			    	count++;
			    } //End While
			}
			else
			{
				driver.findElement(By.xpath("//UIAButton[@label='" + menuOption +"']")).click();
				Thread.sleep(5000);
			}
			
		}//End try
	    catch (Exception e) {
        	throw new Exception("Exception at ObjectProperties cickMenuItem : " + e);
	    }
	    
	    finally	{
	    	Thread.sleep(5000);
    		Log.event("ObjectProperties cickMenuItem", StopWatch.elapsedTime(startTime));
    	}
    	
	} //cickMenuItem
	
	/**
     * getAddedFilesCount : Returns the no of added files
     * @param null
     * @return count of added files
     * @throws Exception 
     */

	public int getAddedFilesCount() throws Exception {
		
		final long startTime = StopWatch.startTime();
		int fileCount = 0;
		
		try	{
				if(Utils.doesElementExist(driver , txtAddedFilesTitle.get(0)))	{
					fileCount = txtAddedFilesTitle.size();
				}
				return fileCount;
			}//End try
	    catch (Exception e) {
        	throw new Exception("Exception at ObjectProperties getAddedFilesCount : " + e);
	    }
		
	    finally	{
    		Log.event("ObjectProperties getAddedFilesCount", StopWatch.elapsedTime(startTime));
    	}
    	
	} //getAddedFilesCount
	
	/**
     * getAddedFilesCount : Returns the index of added file
     * @param added fileName
     * @return index of added file
     * @throws Exception 
     */

	public int getAddedFileIndex(String fileName) throws Exception {
		
		final long startTime = StopWatch.startTime();
		
		try	{
			
			int count = 0;
			int listCount = txtAddedFilesTitle.size();
		    	 
		    while(count < listCount){		
		    	if(txtAddedFilesTitle.get(count).getText().contentEquals(fileName)){
		    		break;
		    	} //End If   
		    	count++;
		    } //End While
		    
		    if(count >= listCount)
		    	return -1;
		    
		    return count;
		    
		}//End try
		
	    catch (Exception e) {
        	throw new Exception("Exception at ObjectProperties getAddedFileIndex : " + e);
	    }
		
	    finally	{
    		Log.event("ObjectProperties getAddedFileIndex", StopWatch.elapsedTime(startTime));
    	}
    	
	} //getAddedFilesCount
	
	/**
     * renameAddedFile : Returns the index of added file
     * @param operation to be performed
     * @param new name for the file
     * @return index of added file
     * @throws Exception 
     */

	public void renameAddedFile(String Dooperation,String newFileName) throws Exception {
		
		final long startTime = StopWatch.startTime();
		
		try	{
			
			int count = 0;		
			int listCount = txtMenuTitle.size();
		    	 
		    while(count < listCount){		
		    	if(txtMenuTitle.get(count).getText().contentEquals(Dooperation)){
		    		txtMenuTitle.get(count).click();
		    		break;
		    	} //End If   
		    	count++;
		    } //End While
		    
		    if(Utils.doesElementExist(driver , txtRenameTitle)) {
		    	
		    	txtRenameEdit.clear();
		    	txtRenameEdit.sendKeys(newFileName);
		    	btnRenameOk.click();
		    	Thread.sleep(5000);
		    }
		    
		}//End try
		
	    catch (Exception e) {
        	throw new Exception("Exception at ObjectProperties renameAddedFile : " + e);
	    }
		
	    finally	{
    		Log.event("ObjectProperties renameAddedFile", StopWatch.elapsedTime(startTime));
    	}
    	
	} //renameAddedFile
	
	/**
     * DeleteAddedFile : Returns the index of added file
     * @param operation to be performed
     * @param new name for the file
     * @return index of added file
     * @throws Exception 
     */

	public void DeleteAddedFile(String Dooperation) throws Exception {
		
		final long startTime = StopWatch.startTime();
		
		try	{
			
			int count = 0;		
			int listCount = txtMenuTitle.size();
		    	 
		    while(count < listCount){		
		    	if(txtMenuTitle.get(count).getText().contentEquals(Dooperation)){
		    		txtMenuTitle.get(count).click();
		    		Thread.sleep(5000);
		    		break;
		    	} //End If   
		    	count++;
		    } //End While
		    
		}//End try
		
	    catch (Exception e) {
        	throw new Exception("Exception at ObjectProperties DeleteAddedFile : " + e);
	    }
		
	    finally	{
    		Log.event("ObjectProperties DeleteAddedFile", StopWatch.elapsedTime(startTime));
    	}
    	
	} //DeleteAddedFile
	
	/**
     * getAddedFilesCount : Returns the no of added files
     * @param name of the added file
     * @param operation to perform on the added file
     * @throws Exception 
     */

	public void changeAddedFile(String fileName,String Dooperation, String newFileName) throws Exception {
		
		final long startTime = StopWatch.startTime();
		
		try	{
			int index = getAddedFileIndex(fileName);
			
			if(index >=0 && Utils.doesElementExist(driver , txtAddedFilesTitle.get(index)))	{
				Utils.longPress(driver,txtAddedFilesTitle.get(index));
				
				if(Dooperation.toUpperCase().contentEquals("RENAME")){
					renameAddedFile(Dooperation,newFileName);
				}
				else if(Dooperation.toUpperCase().contentEquals("DELETE")){
					DeleteAddedFile(Dooperation);
				}
			}
		}//End try
	    catch (Exception e) {
        	throw new Exception("Exception at ObjectProperties changeAddedFile : " + e);
	    }
		
	    finally	{
    		Log.event("ObjectProperties changeAddedFile", StopWatch.elapsedTime(startTime));
    	}
    	
	} //getAddedFilesCount
	
	public void switchToPageForward(String pageName) throws Exception
	{
		Utils.waitForElement(driver, pageProperties);
		
		if(MobileDriverUtils.platform.equalsIgnoreCase("Android"))
		{
			int count = 0;
			int listCount = pageTabs.size();
	    	 
	    	while(count < listCount){		
	    		if(pageTabs.get(count).getText().contentEquals(pageName)){
	    			pageTabs.get(count).click();
	    			break;
	    		} //End If   
	    		count++;
	    		
	    	} //End While
	    	
	    	if(count >= listCount)	{
	    		pageTabs.get(listCount-1).click();
	    		switchToPageForward(pageName);
	    	}
		}
		else
		{
			driver.findElement(By.xpath("//UIATabBar/UIAButton[@label='" + pageName + "']")).click();
		}		
	} //switchToPageForward
	
	public void switchToPageBackward(String pageName) throws Exception {
		Utils.waitForElement(driver, pageProperties);
		int count = 0;
		int listCount = pageTabs.size();
    	 
    	while(count < listCount){		
    		if(pageTabs.get(count).getText().contentEquals(pageName)){
    			pageTabs.get(count).click();
    			break;
    		} //End If   
    		count++;
    		
    	} //End While
    	
    	if(count >= listCount)	{
    		pageTabs.get(0).click();
    		switchToPageBackward(pageName);
    	}
    	
	} //switchToPageBackward
	
}
