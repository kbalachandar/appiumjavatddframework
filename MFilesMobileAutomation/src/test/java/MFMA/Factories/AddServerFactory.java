package MFMA.Factories;

import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;

import com.relevantcodes.extentreports.ExtentTest;

import MFMA.AppLibrary.ResourceString;
import MFMA.Screens.AddServerScreen;
import genericLibrary.Log;
import genericLibrary.MobileDriverUtils;
import genericLibrary.StopWatch;
import genericLibrary.Utils;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class AddServerFactory extends AddServerScreen
{
	RemoteWebDriver driver;
	private boolean pageLoaded = false;
	
	/**
	 * AddServer : Connects to the server page
	 * @param driver : RemoteWebdriver
	 * @throws Exception 
	 */
	public AddServerFactory(RemoteWebDriver driver) throws Exception {
		
		final long startTime = StopWatch.startTime();
		
		try {
			this.driver = driver;
	        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
        }
        catch (Exception e) {
        	throw new Exception("Exception at ConnectServerPage constructor : " + e);
        }
        finally {
        	Log.event("AddServer Constructor", StopWatch.elapsedTime(startTime));
        }
    }
	
	final protected void isLoaded() {
		if (!pageLoaded) {
			
		}
	}
	
	final protected void load(){
		try {
			
			Utils.fluentWait(driver);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		pageLoaded = true;
	}
	
	/**
	 * setServerName : Sets the server
	 * @param setServerName - Name/Ipaddress of the server
	 * @throws Exception 
	 */
    public void setServerName(String strServerName) throws Exception
    {
    	txtServername.clear();
    	txtServername.sendKeys(strServerName);
    }
    
    /**
	 * clickConnect : clicks the connect button
	 * @throws Exception 
	 */
    public void clickConnect() throws Exception
    {
    	btnConnect.click();
    }
    
    /**
	 * connectToServer : Connecting to the server
	 * @param setServerName - Name/Ipaddress of the server
	 * @throws Exception 
	 */
    public void connectToServer(String strServerName) throws Exception
    {
        this.setServerName(strServerName);   
        this.clickConnect();
    }
    
    /**
	 * Tapping Empty Error Dialog Box OK Button
	 */
    public void tapOkButton()
    {
    	btnEmptyErrorOK.click();
    }
    
    /**
	 * Tapping Invalid Error Dialog Box OK Button
	 */
    public void tapInvalidOkButton()
    {
    	btnInvalidErrorOK.click();
    }
    
    /**
	 * Verify Empty Error Message
	 * @throws Exception 
	 */
    public Boolean verifyEmptyError() throws Exception
    {
    	final long startTime = StopWatch.startTime();
    	
    	Utils.waitForElement(driver, txtEmptyErrorMessage);
		try
		{
			if(MobileDriverUtils.platform.equalsIgnoreCase("Android")) {
				if ((txtEmptyErrorMessage.getText()).equalsIgnoreCase(ResourceString.ErrorMessage.ServerIsRequiredAndroid.Value))
					return true;
	    		}
			else	{
				if ((txtEmptyErrorMessage.getText()).equalsIgnoreCase(ResourceString.ErrorMessage.ServerIsRequiredIos.Value))
					return true;
	    		}
	    }//End try
		catch (Exception e) {
			throw new Exception("Exception at AddServerPage.verifyEmptyError : " + e);
		} //End catch
		finally {
			Log.event("AddServerPage.verifyEmptyError.", StopWatch.elapsedTime(startTime));
		} //End finally
		
		return false;
    }
    
    /**
	 * Verify Invalid Error Message
	 * @throws Exception 
	 */
    public Boolean verifyInvalidError() throws Exception
    {
    	final long startTime = StopWatch.startTime();
    	
    	try {
    		Utils.waitForElement(driver, txtInvalidErrorMessage);
    		if(MobileDriverUtils.platform.equalsIgnoreCase("Android")) {
				if ((txtInvalidErrorMessage.getText()).equalsIgnoreCase(ResourceString.ErrorMessage.InvalidServerAndroid.Value))
					return true;
	    		}
			else	{
				if ((txtInvalidErrorMessage.getText()).equalsIgnoreCase(ResourceString.ErrorMessage.InvalidServerIos.Value))
					return true;
	    		}
    	}
    	catch (Exception e) {
			throw new Exception("Exception at AddServerPage.verifyInvalidError : ", e);
		} // End catch

		finally {
			Log.event("AddServerPage.verifyInvalidError :", StopWatch.elapsedTime(startTime));
		}
    	
    	return false;
    }
    
    /**
	 * Navigate to Login Screen
	 * @return instance of Login Screen
	 * @throws Exception 
	 */
    public LoginFactory navigateToLoginPage() throws Exception
    {
    	Log.event("Navigated to Login Page!");
		return (LoginFactory) new LoginFactory(driver).get();
    }
}
