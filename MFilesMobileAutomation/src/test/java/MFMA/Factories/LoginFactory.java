package MFMA.Factories;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.testng.Assert;

import com.relevantcodes.extentreports.ExtentTest;

import MFMA.AppLibrary.ResourceString;
import MFMA.Screens.LoginScreen;
import genericLibrary.Log;
import genericLibrary.MobileDriverUtils;
import genericLibrary.StopWatch;
import genericLibrary.Utils;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class LoginFactory extends LoginScreen 
{
	RemoteWebDriver driver;
	private boolean pageLoaded = false;
	
	/**
	 * AddServer : Connects to the server page
	 * @param driver : RemoteWebdriver
	 * @throws Exception 
	 */
	public LoginFactory(RemoteWebDriver driver) throws Exception {
		
		final long startTime = StopWatch.startTime();
		
		try {
			this.driver = driver;
	        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
        }
        catch (Exception e) {
        	throw new Exception("Exception at ConnectServerPage constructor : " + e);
        }
        finally {
        	Log.event("AddServer Constructor", StopWatch.elapsedTime(startTime));
        }
    }
	
	final protected void isLoaded() {
		try {
			pageLoaded = Utils.waitForElement(driver, txtUsername);
		}
		catch (TimeoutException e) {
			throw e;
		}
	}
	
	final protected void load(){
		try {
			
			Utils.fluentWait(driver);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		pageLoaded = true;
	}
	
	/**
	 * Verifying whether page loaded properly
	 * @return true if page loaded
	 */
	public Boolean pageLoaded()
	{
		isLoaded();
		return pageLoaded;
	}
	/**
	 * Tapping Empty Error Dialog Box OK Button
	 */
	public void tapOkButton()
    {
    	btnEmptyErrorOK.click();
    }
	
	/**
	 * Tapping Invalid Error Dialog Box OK Button
	 */
    public void tapInvalidOkButton()
    {
    	btnInvalidErrorOK.click();
    }
    
	/**
	 * setUserName : Sets the user name
	 * @param userName - Name of the user
	 * @throws Exception 
	 */
    public void setUserName(String strUserName) throws Exception
    {
    	txtUsername.clear();
    	txtUsername.sendKeys(strUserName);
    }

    /**
	 * setPassword : Sets the password
	 * @param password - Password
	 * @throws Exception 
	 */
    public void setPassword(String password) throws Exception
    {
    	txtPassword.clear();
    	txtPassword.sendKeys(password);
    }

    /**
	 * clickLogin : Clicks login button
	 * @throws Exception 
	 */
    public void clickLogin() throws Exception
    {
    	btnLogin.click();
    }

    /**
	 * setUserName : Sets the user name
	 * @param userName - Name of the user
	 * @param password - Password of the user
	 * @throws Exception 
	 */
    public void loginToMfiles(String userName,String strPasword) throws Exception
    {
        this.setUserName(userName); //Fill user name
        this.setPassword(strPasword); //Fill password
        if(MobileDriverUtils.platform.equalsIgnoreCase("Android"))
    	{
        	driver.navigate().back();
    	}
        this.clickLogin(); //Click Login button
    }
    
    /**
	 * Verify Empty Error Message
	 * @throws Exception 
	 */
    public Boolean verifyEmptyError() throws Exception
    {
    	final long startTime = StopWatch.startTime();
    	
    	try {
    		Utils.waitForElement(driver, txtEmptyErrorMessage);
    		if(MobileDriverUtils.platform.equalsIgnoreCase("Android")) {
				if ((txtEmptyErrorMessage.getText()).equalsIgnoreCase(ResourceString.ErrorMessage.UserNameRequiredAndroid.Value))
					return true;
	    		}
			else	{
				if ((txtEmptyErrorMessage.getText()).equalsIgnoreCase(ResourceString.ErrorMessage.UserNameRequiredIos.Value))
					return true;
	    		}
    	}
    	catch (Exception e) {
			throw new Exception("Exception at LoginPage.verifyEmptyError : ", e);
		} // End catch

		finally {
			Log.event("LoginPage.verifyEmptyError :", StopWatch.elapsedTime(startTime));
		}
    	
    	return false;
    	
    } //End verifyEmptyError
    
    /**
	 * verifyInvalidError : To verify invalid error message
	 * @param None
	 * @return Returns true if error message with message thrown, if returns false.
	 * @throws Exception 
	 */
    public Boolean verifyInvalidError() throws Exception {
    	
    	final long startTime = StopWatch.startTime();
    	
    	try {
    		
    		Utils.waitForElement(driver, txtInvalidErrorMessage);
    		Utils.fluentWait(driver);

    		if(MobileDriverUtils.platform.equalsIgnoreCase("Android")) {
				if ((txtInvalidErrorMessage.getText().trim()).equalsIgnoreCase(ResourceString.ErrorMessage.InvalidLoginAndroid.Value))
					return true;
	    		}
			else	{
				if ((txtInvalidErrorMessage.getText().trim()).equalsIgnoreCase(ResourceString.ErrorMessage.InvalidLoginIos.Value))
					return true;
	    		}
    	}
    	catch (Exception e) {
			throw new Exception("Exception at LoginPage.verifyInvalidError : ", e);
		} // End catch

		finally {
			Log.event("LoginPage.verifyInvalidError :", StopWatch.elapsedTime(startTime));
		}
    	
    	return false;
    	
    } //End verifyInvalidError
    
    /**
	 * Navigate to Vault Page
	 * @return instance of Vault Page
	 * @throws Exception 
	 */
    public VaultSelectionFactory navigateToVaultPage() throws Exception
    {
    	Log.event("Navigated to Vault Screen!");
		return (VaultSelectionFactory) new VaultSelectionFactory(driver).get();
    }
}
