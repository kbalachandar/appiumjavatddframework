package MFMA.AppLibrary;


public class ResourceString {
	
	 public enum ErrorMessage { //Object types
		
		 ServerIsRequiredAndroid			("Server is required."),
		 UserNameRequiredAndroid			("Username is required."),
		 ServerIsRequiredIos				("Server is required"),
		 UserNameRequiredIos				("Username is required"),
		 NoVaultsFound						("No vaults found"),
		 InvalidServerIos					("There was a problem communicating with the secure web proxy server (HTTPS)."),
		 InvalidServerAndroid				("Please check your username, password and server address, and confirm you have a valid license on the server."),
		 InvalidLoginAndroid				("Please check your username, password and server address, and confirm you have a valid license on the server."),
		 InvalidLoginIos					("Login to application failed");
	 
		public String Value;
	 
		ErrorMessage(String caption) {
			this.Value = caption;
		}
		
 	}//end enum ErrorMessage
	 
	public enum MenuItems { //Object types
			
		 SFDToMFD					("Convert to Multi-File Document"),
		 MFDToSFD					("Convert to Single-File Document");
		 
		public String Value;
	 
		MenuItems(String caption) {
			this.Value = caption;
		}
		
 	}//end enum ErrorMessage
	 
	 public enum PropertyPages { //Object types
			
		 Relation					("Reationship"),
		 Property					("Preperties"),
		 Comment					("Comment"),
		 Workflow					("Workflow");
		 
		public String Value;
	 
		PropertyPages(String caption) {
			this.Value = caption;
		}
		
 	}//end enum PropertyPages
	 
	 public enum ViewNames { //Default view names
			
		 AssignedToMe					("Assigned to me"),
		 RecentAccessAndroid			("Recently accessed"),
		 RecentAccessIos				("Recent"),
		 Offline						("Offline"),
		 AllView						("All views"),
		 Search							("Search"),
		 Favorites						("Favorites");
		 
		public String Value;
	 
		ViewNames(String caption) {
			this.Value = caption;
		}
		
 	}//end enum ViewNames
	
}//ResourceString
	

	
	
 
