package MFMA.Tests;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.concurrent.ConcurrentHashMap;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.xml.XmlTest;

import com.relevantcodes.extentreports.DisplayOrder;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.NetworkMode;

import MFMA.AppLibrary.ResourceString;
import MFMA.Factories.*;
import MFMA.Screens.AddServerScreen;
import MFMA.Screens.LoginScreen;
import genericLibrary.BaseTest;
import genericLibrary.DataUtils;
import genericLibrary.EmailReport;
import genericLibrary.EnvironmentPropertiesReader;
import genericLibrary.Log;
import genericLibrary.MobileDriverUtils;
import genericLibrary.Utils;

@Listeners(EmailReport.class)
public class NSmokeTestCases extends BaseTest {
	
	public static String xlTestDataWorkBook = null;
	public static String userName = null;
	public static String password = null;
	public static String testVault = null;
	public static String serverName = null;
	public static String objectType = null;
	public static String className = null;
	public static String templateName = null;
	public static String pName = null;
	public static String pAssignment = null;
	public static String pAssigned = null;
	public static String pDeadline = null;
	public static String csearchKeyword = null;
	public static String wsearchKeyword = null;
	public static String citemName = null;
	public static String witemName = null;
	public static String workflowName = null;
	public static String workflowState = null;
	public static Boolean testResult = null;
	public RemoteWebDriver driver = null;
	public ITestResult result;
	public static String deviceName = null;
	
	/**
	 * init : Before Class method to perform initial operations.
	 */
	@BeforeClass (alwaysRun=true)
	public void init() throws Exception {
		try {
			MobileDriverUtils.startHub();
			MobileDriverUtils.startNode();
			Thread.sleep(10000);
			driver = MobileDriverUtils.getDriver();
			Utils.popupHandler(driver);
			deviceName = MobileDriverUtils.testName;
		} //End try
		
		catch(Exception e) {
				Log.exception(e);
		} //End catch
		
	} //End init
	
	/**
	 * writeResult : AfterMethod method to get test result status of last executed test case
	 */
	@AfterMethod
	public void writeResult(ITestResult result) throws Exception
	{
		if(result.getStatus() == ITestResult.SUCCESS)
		{
			testResult = true;
		}
		else
		{
			testResult = false;
			driver.quit();
			driver = MobileDriverUtils.getDriver();
			Utils.popupHandler(driver);
		}
	}
	
	/**
	 * TC_001 : Connect to server
	 */
	@Test(description = "Connect to server with empty string")
	public void SmokeCaseTest1_1() throws Exception {
		
		ExtentTest extentedReport = Log.testCaseInfo("MFMATest001", "Connect to server with empty string", "MFMA_SmokeTest", deviceName);
		try {
			AddServerFactory addServerPage = new AddServerFactory(driver);
			Log.message("Step-1: MFiles Mobile Application launched successfully!!", driver, extentedReport);
			
			addServerPage.connectToServer("");
			Log.message("Step-2: Entered nothing in the server textbox", driver, extentedReport);
			
			if (addServerPage.verifyEmptyError())
				Log.pass("Step-3: Verified Empty Error Message", driver, extentedReport);
			else
				Log.fail("Test case Failed. Error message is not same as expected", driver, extentedReport);
			
			addServerPage.tapOkButton();
			Log.message("Step-4: Tapped Empty Error Message Dialog Box", driver, extentedReport);
			
			Log.testCaseResult(extentedReport);
		}
		catch (Exception e) {
			Log.exception(e, driver, extentedReport);
		} //End catch
		
		finally {
			Log.endTestCase(extentedReport);
		} //End finally
		
	} //End SmokeCaseTest1_1

	/**
	 * TC_002 : Connect to Server
	 */
	@Test(description = "Connect to server with invalid address")
	public void SmokeCaseTest1_2() throws Exception {
		
		ExtentTest extentedReport = Log.testCaseInfo("MFMATest002", "Connect to server with invalid address", "MFMA_SmokeTest", deviceName);
		try {
				
			AddServerFactory addServerPage = new AddServerFactory(driver);
			Log.message("Step-1: MFiles Mobile Application launched successfully!!", driver, extentedReport);
			
			addServerPage.connectToServer("fghgh");
			Log.message("Step-2: Entered invalid text in the server textbox", driver, extentedReport);
			
			if (addServerPage.verifyInvalidError())
				Log.pass("Step-3: Verified Invalid Error Message on logging in with invalid text in the server textbox", driver, extentedReport);
			else
				Log.fail("Test case Failed. Error message is not same as expected.", driver, extentedReport);
			
			addServerPage.tapInvalidOkButton();
			Log.message("Step-4: Tapped Invalid Error Message Dialog Box", driver, extentedReport);
			
			Log.testCaseResult(extentedReport);
		}
		catch (Exception e) {
			Log.exception(e, driver, extentedReport);
		} //End catch
		
		finally {
			Log.endTestCase(extentedReport);
		} //End finally
		
	} //End SmokeCaseTest1_2
	
	/**
	 * TC_003 : Login to application
	 */
	@Test(description = "Login to application with empty username and password")
	public void SmokeCaseTest1_3() throws Exception {
		
		ExtentTest extentedReport = Log.testCaseInfo("MFMATest003", "Login to application with empty username and password", "MFMA_SmokeTest", deviceName);
		
		HashMap <String, String> testData = DataUtils.testDatabyID("MFMATest003", "SmokeTestCases_POC");
		serverName = testData.get("ServerName").toString();
		
		try {
				AddServerFactory addServerPage = new AddServerFactory(driver);
				addServerPage.connectToServer(serverName);
				
				LoginFactory loginPage = new LoginFactory(driver);
				loginPage.loginToMfiles("", "");
				Log.message("step-1: Logging into MFiles with empty string", driver, extentedReport);
				
				if (loginPage.verifyEmptyError())
					Log.pass("Step-2: Verified Empty Error Message on logging in with empty user name and password.", driver, extentedReport);
				else
					Log.fail("Test case Failed. Error message is not same as expected.", driver, extentedReport);
				
				loginPage.tapOkButton();
				Log.message("Step-3: Tapped Empty Error Message Dialog Box", driver, extentedReport);
				
				Log.testCaseResult(extentedReport);
		}
		catch (Exception e) {
			Log.exception(e, driver, extentedReport);
		} //End catch
		
		finally{
			Log.endTestCase(extentedReport);
		} //End finally
		
	} //End SmokeCaseTest1_3
	
	/**
	 * TC_004 : Login to application
	 */
	@Test(description = "Login to application with invalid username")
	public void SmokeCaseTest1_4() throws Exception {
		
		ExtentTest extentedReport = Log.testCaseInfo("MFMATest004", "Login to application with invalid username", "MFMA_SmokeTest", deviceName);
		try {
			
			if(!testResult)
			{
				AddServerFactory addServerPage = new AddServerFactory(driver);
				addServerPage.connectToServer(serverName);
			}
			
			LoginFactory loginPage = new LoginFactory(driver);
			loginPage.loginToMfiles("asfb4565", "test");
			Log.message("step-1: Logging into MFiles with invalid username", driver, extentedReport);
			
			if (loginPage.verifyInvalidError())
				Log.pass("Step-2: Verified Invalid Error Message on logging in with empty user name and password.", driver, extentedReport);
			else
				Log.fail("Test case Failed. Error message is not same as expected.", driver, extentedReport);
			
			loginPage.tapInvalidOkButton();
			Log.message("Step-3: Tapped Invalid Error Message Dialog Box", driver, extentedReport);
			
			Log.testCaseResult(extentedReport);
		}
		catch (Exception e) {
			Log.exception(e, driver, extentedReport);
		} //End catch
		
		finally {
			Log.endTestCase(extentedReport);
		} //End finally
		
	} //End SmokeCaseTest1_4
	
	/**
	 * tearDown : After Class method to perform final operations.
	 */
	@AfterClass
	public void tearDown() throws Exception
	{
		driver.quit();
	}
		 
}
