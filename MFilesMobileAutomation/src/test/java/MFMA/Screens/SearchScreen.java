package MFMA.Screens;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.LoadableComponent;

import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;

public class SearchScreen extends LoadableComponent<SearchScreen>
{
	@AndroidFindBy(id="search_input_phone")
	@iOSFindBy(xpath="//UIAApplication[1]/UIAWindow[1]/UIASearchBar[1]/UIASearchBar[1]")
    protected WebElement editorSearch;
    
	@AndroidFindBy(id="menu_search_settings")
    protected WebElement menuSearchFilter;
	
	@AndroidFindBy(id="ex_component_object_list_items")
	@iOSFindBy(className="UIATableView")
    protected WebElement objectListView;
	
	@AndroidFindBy(id="search_settings_bar")
    protected WebElement searchSettings;
	
	@AndroidFindBy(id="object_list_group_title")
    protected WebElement listGroupTitle;
    
	@AndroidFindBy(id="object_list_group_count")
    protected WebElement countGroupTitle;
	
	@AndroidFindBy(xpath="//search_type_filter_item//component_search_settings_item_spinner")
    protected WebElement spinnerSearchType;

	@AndroidFindBy(xpath="//search_from_filter_item//component_search_settings_item_spinner")
    protected WebElement spinnerSearchFrom;
    
	@AndroidFindBy(id="component_object_list_item_title")
	@iOSFindBy(className="UIATableCell")
    protected List<WebElement> titleObjectsName;
    
	@iOSFindBy(xpath="//UIAButton[@name='Search']")
    protected WebElement btnKeyboardSearch;
	
	@Override
	protected void isLoaded() throws Error {
		// TODO Auto-generated method stub
	}

	@Override
	protected void load() {
		// TODO Auto-generated method stub
	}

}
