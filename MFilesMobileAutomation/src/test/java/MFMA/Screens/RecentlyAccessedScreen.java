package MFMA.Screens;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.LoadableComponent;

import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;

public class RecentlyAccessedScreen extends LoadableComponent<RecentlyAccessedScreen>
{
	@FindBy(id="component_object_list_item_title")
    protected List<WebElement> titleObjectsName;
    
	@AndroidFindBy(xpath="//android.widget.ImageButton[@content-desc='Navigate up']")
	@iOSFindBy(xpath="//UIAButton[1]")
    protected WebElement btnMenu;
	
	@Override
	protected void isLoaded() throws Error {
		// TODO Auto-generated method stub
	}

	@Override
	protected void load() {
		// TODO Auto-generated method stub
	}

}
