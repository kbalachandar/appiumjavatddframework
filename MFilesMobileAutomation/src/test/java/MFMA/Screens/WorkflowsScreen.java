package MFMA.Screens;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.LoadableComponent;

import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;

public class WorkflowsScreen extends LoadableComponent<WorkflowsScreen> {
	
	@AndroidFindBy(id="fragment_properties_workflow_name")
	@iOSFindBy(xpath="//UIATableCell[@name='WORKFLOW']")
    protected WebElement spinnerWorkflowName;
    
	@AndroidFindBy(id="fragment_properties_workflow_value")
	@iOSFindBy(xpath="//UIATableCell[@name='STATE']")
    protected WebElement spinnerWorkflowValue;
    
	@AndroidFindBy(id="fragment_properties_workflow_comment")
    protected WebElement editorWorkflowComment;
    
	@AndroidFindBy(id="fragment_error_title")
	@iOSFindBy(xpath="//UIAScrollView[1]/UIAStaticText[2]")
    protected WebElement errorTitle;
    
	@AndroidFindBy(id="fragment_error_ok")
    protected WebElement errorok;
    
	@AndroidFindBy(className="android.widget.CheckedTextView")
    protected List<WebElement> optionsText;
    
	@AndroidFindBy(id="alertTitle")
	@iOSFindBy(className="UIATableView")
    protected WebElement spinnerTitle;
    
	@AndroidFindBy(id="menu_item_save")
	@iOSFindBy(xpath="//UIAButton[@label='mfiles icon done']")
    protected WebElement btnSave;
    
	@AndroidFindBy(id="message")
    protected WebElement discardMessage;
    
	@AndroidFindBy(id="button1")
    protected WebElement btnYes;    
    
	@AndroidFindBy(id="view_dialog_credentials_title")
    protected WebElement  txtTitle;
    
	@AndroidFindBy(id="view_dialog_credentials_fixed_prompt_info")
    protected WebElement  simpleEsignDescription;
    
	@AndroidFindBy(id="view_dialog_credentials_username")
    protected WebElement editUserName;
    
	@AndroidFindBy(id="view_spinner_list_view_item_text")
    protected List<WebElement> spinnerReason;
    
	@AndroidFindBy(id="view_dialog_credentials_password")
	@iOSFindBy(xpath="//UIATableCell[5]/UIASecureTextField[1]")
    protected WebElement editUserPassword;
    
	@AndroidFindBy(id="view_dialog_credentials_ok")
	@iOSFindBy(xpath="//UIAButton[@label='mfiles icon done']")
    protected WebElement btnSign;
    
	@AndroidFindBy(id="view_dialog_credentials_cancel")
    protected WebElement btnCancel;
    
	@AndroidFindBy(id="view_dialog_credentials_meaning")
    protected WebElement txtMeaning;
    
	@Override
	protected void isLoaded() throws Error {
		// TODO Auto-generated method stub
	}

	@Override
	protected void load() {
		// TODO Auto-generated method stub
	}

}
