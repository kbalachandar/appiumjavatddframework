package genericLibrary;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import genericLibrary.Log;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.xml.XmlTest;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;

public class Utils {
	
	public static int snoozeTime = 0;
	public static int snoozeIdx = 0;
	
	private static EnvironmentPropertiesReader configProperty = EnvironmentPropertiesReader.getInstance();
	static XmlTest test = Reporter.getCurrentTestResult().getTestContext().getCurrentXmlTest();
	static String platform = test.getParameter("platform");
	static int minTimeout = Integer.parseInt(configProperty.getProperty("minElementWait"));
	static int maxTimeout = Integer.parseInt(configProperty.getProperty("maxElementWait"));
	static int extraTimeout = Integer.parseInt(configProperty.getProperty("extraElementWait"));
	
	/**
	 * fluentWait: This method is to wait until the current progress gets completed
	 * @param driver
	 * @return None
	 * @throws Exception
	 */
	public static void fluentWait(WebDriver driver) throws Exception {

		final long startTime = StopWatch.startTime();
		
		try {
			
			int snooze = 0;
			
			while (driver.findElement(By.id("progress_spinner")).isDisplayed() && snooze<60) {
				Thread.sleep(1000);
				snooze++;
			}

		} // End try
		catch (Exception e) {
				return;
		} // End catch

		finally {
			Log.event("Utils.fluentWait :", StopWatch.elapsedTime(startTime));
		}

	} // End fluentWait
	
	/**
	 * waitForElement: Method to wait for the Web Element
	 * @param driver: RemoteWebDriver Object
	 * @param pElement: element to be exist
	 * @return Boolean: True if the element exists, false otherwise
	 * @throws Exception
	 */
    public static Boolean waitForElement(RemoteWebDriver driver, WebElement pElement)
    {
    	Boolean isFound = false;
    	try
    	{
            int _TimeToWait = maxTimeout;
            for (int i = 0; i < _TimeToWait; i++)
            {
                Thread.sleep(1000); //Wait for 1 second

                try
                {
                    isFound = pElement.isDisplayed();
                    if (isFound) break;
                    System.out.println("Searching the Element..." + pElement);
                }
                catch (Exception ex)
                {
                	System.out.println(ex.getMessage());
                }
            }
    	}
        catch(Exception e)
    	{
        	isFound = false;
			Log.event("Unable to find a element after waiting for " + maxTimeout);
    	}
    	return isFound;
    }
    
    /**
	 * doesElementExist: Method to verify for the existence of Web Element
	 * @param driver: RemoteWebDriver Object
	 * @param pElement: element to be exist
	 * @return Boolean: True if the element exists, false otherwise
	 * @throws Exception
	 */
    public static Boolean doesElementExist(RemoteWebDriver driver, WebElement pElement)
    {
    	Boolean isFound = false;
    	try
    	{
            int _timeToWait = minTimeout;
            for (int i = 0; i < _timeToWait; i++)
            {
                Thread.sleep(1000); //Wait for 1 second

                try
                {
                    isFound = pElement.isDisplayed();
                    if (isFound) break;
                    System.out.println("Searching the Element..." + pElement);
                }
                catch (Exception ex)
                {
                	System.out.println(ex.getMessage());
                }
            }
    	}
        catch(Exception e)
    	{
        	isFound = false;
			Log.event("Unable to find a element after ");
    	}
    	return isFound;
    }
    
    /**
	 * doesElementExist: Method to verify for the existence of child Web Element
	 * @param driver: RemoteWebDriver Object
	 * @param pElement: parent element
	 * @param by: locator of the child element
	 * @return Boolean: True if the element exists, false otherwise
	 * @throws Exception
	 */
    public static Boolean doesElementExist(RemoteWebDriver driver, WebElement pElement, By by)
    {
    	Boolean isFound = false;
    	try
    	{
            int _timeToWait = minTimeout;
            for (int i = 0; i < _timeToWait; i++)
            {
                Thread.sleep(1000); //Wait for 1 second

                try
                {
                    WebElement element  = pElement.findElement(by);
                    if (element.isDisplayed()) break;
                    Log.event("Searching the Element..." + pElement);
                }
                catch (Exception ex)
                {
                	Log.event(ex.getMessage());
                }
            }
    	}
        catch(Exception e)
    	{
        	isFound = false;
			Log.event("Unable to find a element after ");
    	}
    	return isFound;
    }
    
    /**
	 * doesElementExist: This method is to check if elementexists
	 * @param driver
	 * @param pElement
	 * @param pElement
	 * @return _timeToWait
	 * @throws Exception
	 */
    public static Boolean doesElementExist(RemoteWebDriver driver, WebElement pElement, int _timeToWait) throws Exception {
    	
    	final long startTime = StopWatch.startTime();
    	
    	try {
            
            for (int i = 0; i < _timeToWait; i++) {
                Thread.sleep(1000); //Wait for 1 second

                try {
                    if (pElement.isDisplayed()) return true;
                }
                catch (Exception ex) {  }
            }
    	}
        catch(Exception e) {
        	return false;
	   	}
    	
    	finally {
			
			Log.event("Utils.fluentWait : Fluent wait operation completed", StopWatch.elapsedTime(startTime));
		}
    	
    	return false;
    }
    /**
	 * doesElementExist: Method to verify for the non existence of the Web Element
	 * @param driver: RemoteWebDriver Object
	 * @param pElement: element not to be existed
	 * @return Boolean: True if the element not exists, false otherwise
	 */
    public static Boolean doesElementNotExist(RemoteWebDriver driver, WebElement pElement)
    {
    	Boolean isNotFound = false;
    	try
    	{
            int _timeToWait = maxTimeout;
            for (int i = 0; i < _timeToWait; i++)
            {
                Thread.sleep(1000); //Wait for 1 second

                try
                {
                	isNotFound = pElement.isDisplayed();
                    if (!isNotFound) break;
                    Log.event("Searching the Element..." + pElement);
                }
                catch (Exception ex)
                {
                	isNotFound=true;
                	Log.event(ex.getMessage());
                }
            }
    	}
        catch(Exception e)
    	{
        	isNotFound = true;
			Log.event("Unable to find a element after ");
    	}
    	return isNotFound;
    }
    
    /**
	 * swipe: To swipe across elements
	 * @param driver: RemoteWebDriver Object
	 * @param pFromElement: Starting Element
	 * @param pToElement: Ending Element
	 * @param duration: amount of time in milliseconds for the entire swipe action
	 */
    @SuppressWarnings("unchecked")
	public static void swipe(RemoteWebDriver driver, WebElement pFromElement, WebElement pToElement, int duration)
    {
        int _startX = pFromElement.getLocation().x + pFromElement.getSize().width / 2;
        int _startY = pFromElement.getLocation().y + pFromElement.getSize().height / 2;
        int _endX = pToElement.getLocation().x + pFromElement.getSize().width / 2;
        int _endY = pToElement.getLocation().y;

        if (platform.equalsIgnoreCase("ANDROID"))
        {
            ((AndroidDriver<WebElement>)driver).swipe(_startX, _startY, _endX, _endY, duration);
        }
        else
        {
        	((IOSDriver<IOSElement>)driver).swipe(_startX, _startY, _endX, _endY, duration);
        }
    }
    
    public static void popupHandler(RemoteWebDriver driver)
    {
    	if(!platform.equalsIgnoreCase("ANDROID"))
    		driver.findElement(By.xpath("//UIAButton[@label='OK']")).click();
    }
    
    /**
	 * pressKeyCode: This method is press key in the mobile app
	 * @param driver
	 * @param keycode keycode to press
	 * @return None
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public static void pressKeyCode(RemoteWebDriver driver, int keycode) throws Exception {
		
		final long startTime = StopWatch.startTime();
		
		try {
			
			AndroidDriver androidDriver = (AndroidDriver)driver;
    		androidDriver.pressKeyCode(keycode);

		} // End try
		catch (Exception e) {
			throw new Exception("Exception at MobileDriverUtils.pressKeyCode : ", e);
		} // End catch

		finally {
			Log.event("MobileDriverUtils.pressKeyCode :", StopWatch.elapsedTime(startTime));
		}
	}
	
	/**
	 * pressKeyCode: This method is to long press on the webelement
	 * @param driver
	 * @param webelement to long press
	 * @return None
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public static void longPress(RemoteWebDriver driver, WebElement wElement) throws Exception {
		
		final long startTime = StopWatch.startTime();
		
		try {
			
			AndroidDriver androidDriver = (AndroidDriver)driver;
			TouchAction tAction = new TouchAction(androidDriver);
			tAction.longPress(wElement).release().perform();
    			
		} // End try
		catch (Exception e) {
			throw new Exception("Exception at MobileDriverUtils.longPress : ", e);
		} // End catch

		finally {
			Log.event("MobileDriverUtils.longPress :", StopWatch.elapsedTime(startTime));
		}
	}//End longPress
	
	/**
	 * getCurrentDateTime : This method is to get current date and time
	 * @param None
	 * @return Current date and time in the string format
	 * @throws Exception
	 */
	public static String getCurrentDateTime() {
	
		try {
			
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			Date date = new Date();
			return(dateFormat.format(date));
			
		} //End try
		
		catch (Exception e) {
			throw e;
		} //End catch	
	
	} //End getCurrentDateTime
    
}
