package genericLibrary;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.testng.Reporter;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.xml.XmlTest;

import com.relevantcodes.extentreports.DisplayOrder;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.NetworkMode;

public class BaseTest {
	protected static ExtentReports extent;
		
	@BeforeSuite(alwaysRun = true)
	public void beforeSuite() {
		extent = new ExtentReports("./target/surefire-reports/AutomationExtentReport"+MobileDriverUtils.test.getName()+".html", true);
	}

	/*
	 * After suite will be responsible to close the report properly at the end You an have another afterSuite as well in the derived class and this one will be called in the end making it the last
	 * method to be called in test exe
	 */
	@AfterSuite
	public void afterSuite() throws IOException {
		extent.flush();
	}
}
