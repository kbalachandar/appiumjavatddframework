//
// Created by Tomi Pukkila on 22.5.2014.
// Copyright (c) 2014 M-Files. All rights reserved.
//

//! Folder content item type enumeration.
/*!
 Documentation:
 http://www.m-files.com/mfws/enumerations/mffoldercontentitemtype.html
 */
typedef NS_ENUM( int32_t, MFFolderContentItemType )
{
    MFFolderContentItemTypeUnknown = 0,
    MFFolderContentItemTypeView = 1,
    MFFolderContentItemTypePropertyFolder = 2,
    MFFolderContentItemTypeTraditionalFolder = 3,
    MFFolderContentItemTypeObjectVersion = 4
};