package MFMA.Tests;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.concurrent.ConcurrentHashMap;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.xml.XmlTest;

import com.relevantcodes.extentreports.DisplayOrder;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.NetworkMode;

import MFMA.AppLibrary.ResourceString;
import MFMA.Factories.*;
import MFMA.Screens.AddServerScreen;
import MFMA.Screens.LoginScreen;
import genericLibrary.BaseTest;
import genericLibrary.DataUtils;
import genericLibrary.EmailReport;
import genericLibrary.EnvironmentPropertiesReader;
import genericLibrary.Log;
import genericLibrary.MobileDriverUtils;
import genericLibrary.Utils;

@Listeners(EmailReport.class)
public class SmokeTestCases extends BaseTest {
	
	public static String userName = null;
	public static String password = null;
	public static String testVault = null;
	public static String serverName = null;
	public static String objectType = null;
	public static String className = null;
	public static String templateName = null;
	public static String pName = null;
	public static String pAssignment = null;
	public static String pAssigned = null;
	public static String pDeadline = null;
	public static String searchKeyword = null;
	public static String itemName = null;
	public static String workflowName = null;
	public static String workflowState = null;
	public static Boolean testResult = null;
	public RemoteWebDriver driver = null;
	public ITestResult result;
	public static String deviceName = null;
	
	/**
	 * init : Before Class method to perform initial operations.
	 */
	@BeforeClass (alwaysRun=true)
	public void init() throws Exception {
		
		try {
			MobileDriverUtils.startHub();
			MobileDriverUtils.startNode();
			Thread.sleep(10000);
			driver = MobileDriverUtils.getDriver();
			Utils.popupHandler(driver);
			deviceName = MobileDriverUtils.testName;			
		} //End try
		
		catch(Exception e) {
			if (e.getClass().toString().contains("NullPointerException")) 
				throw new Exception ("Test data sheet does not exists.");
			else {
				System.out.println(e);
				throw e;
			}
		} //End catch
		
	} //End init
	
	/**
	 * writeResult : AfterMethod method to get the test result status of last executed test case
	 */
	@AfterMethod
	public void writeResult(ITestResult result) throws Exception
	{
		if(result.getStatus() == ITestResult.SUCCESS)
		{
			testResult = true;
		}
		else
		{
			testResult = false;
			driver.quit();
			driver = MobileDriverUtils.getDriver();
			Utils.popupHandler(driver);
		}
	}
	
	/**
	 * TC_001 : Connect to Server
	 */
	@Test(description = "Connect to server with valid address")
	public void SmokeCaseTest1_1() throws Exception {
		
		ExtentTest extentedReport = Log.testCaseInfo("MFMATest001", "Connect to server with valid address", "MFMA_SmokeTest", deviceName);
		
		HashMap <String, String> testData = DataUtils.testDatabyID("MFMATest003", "SmokeTestCases_POC");
		serverName = testData.get("ServerName").toString();
		
		try {

			AddServerFactory addServerPage = new AddServerFactory(driver);
			Log.message("Step-1: MFiles Mobile Application launched successfully!!", driver, extentedReport);
			
			addServerPage.connectToServer(serverName);
			Log.message("Step-2: Entered nothing in the server testbox", driver, extentedReport);
			
			LoginFactory loginPage = addServerPage.navigateToLoginPage();
			if(!loginPage.pageLoaded())
				Log.fail("Login Screen not loaded", driver, extentedReport);
			Log.pass("Step-3: Successfully Navigated to Login Page", driver, extentedReport);
			
			Log.testCaseResult(extentedReport);
		}
		catch (Exception e) {
			Log.exception(e, driver, extentedReport);
		} //End catch
		
		finally {
			Log.endTestCase(extentedReport);
		} //End finally
		
	} //End SmokeCaseTest1_1
	
	/**
	 * TC_002 : Login to application
	 */
	@Test(description = "Login to application with valid credentials")
	public void SmokeCaseTest1_2() throws Exception {
		
		ExtentTest extentedReport = Log.testCaseInfo("MFMATest002", "Login to application with valid credentials", "MFMA_SmokeTest", deviceName);
		
		HashMap <String, String> testData = DataUtils.testDatabyID("MFMATest006", "SmokeTestCases_POC");
		userName = testData.get("UserName").toString();
		password = testData.get("Password").toString();
		
		try {
			if(!testResult)
			{
				AddServerFactory addServerPage = new AddServerFactory(driver);
				addServerPage.connectToServer(serverName);
			}
			
			LoginFactory loginPage = new LoginFactory(driver);
			loginPage.loginToMfiles(userName, password);
			Log.message("step-1: Logging into MFiles with valid credentials", driver, extentedReport);
			
			Vault vault = loginPage.navigateToVaultPage();
			if(!vault.pageLoaded())
				Log.fail("Vault Screen not loaded", driver, extentedReport);
			Log.pass("step-2: Navigated to Vault Page", driver, extentedReport);
			
			Log.testCaseResult(extentedReport);
		}
		catch (Exception e) {
			Log.exception(e, driver, extentedReport);
		} //End catch
		
		finally {
			Log.endTestCase(extentedReport);
		} //End finally
		
	} //End SmokeCaseTest1_2
	
	/**
	 * TC_003 : Vault Selection
	 */
	@Test(description = "Vault Selection")
	public void SmokeCaseTest1_3() throws Exception {
		
		ExtentTest extentedReport = Log.testCaseInfo("MFMATest003", "Login to application with valid credentials", "MFMA_SmokeTest", deviceName);
		
		HashMap <String, String> testData = DataUtils.testDatabyID("MFMATest007", "SmokeTestCases_POC");
		testVault = testData.get("vaultName").toString();
		
		try {
			
			if(!testResult)
			{
				AddServerFactory addServerPage = new AddServerFactory(driver);
				addServerPage.connectToServer(serverName);
				LoginFactory loginPage = addServerPage.navigateToLoginPage();
				loginPage.loginToMfiles(userName, password);
			}
		
			Vault vault = new Vault(driver);
			Log.message("Step-1: Started with Vault Screen", driver, extentedReport);
			
			vault.selectVault(testVault);
			Log.message("Step-2: Selecting Vault", driver, extentedReport);
			
			VaultHome vaultHome = vault.navigateToVaultHomeScreen();
			if(!vaultHome.pageLoaded())
				Log.fail("Vault Home not loaded", driver, extentedReport);
			Log.pass("Step-3: Navigated to Vault Home Screen Successfully!!", driver, extentedReport);
				
			
			Log.testCaseResult(extentedReport);
		}
		catch (Exception e) {
			Log.exception(e, driver, extentedReport);
		} //End catch
		
		finally {
			Log.endTestCase(extentedReport);
		} //End finally
		
	} //End SmokeCaseTest1_3
	
	/**
	 * TC_004 : Object Creation
	 */
	@Test(description = "Object Creation")
	public void SmokeCaseTest1_4() throws Exception {
		
		ExtentTest extentedReport = Log.testCaseInfo("MFMATest004", "Creating Object", "MFMA_SmokeTest", deviceName);
		
		HashMap <String, String> testData = DataUtils.testDatabyID("MFMATest008", "SmokeTestCases_POC");
		objectType = testData.get("objectType").toString();
		className = testData.get("className").toString();
		templateName = testData.get("templateName").toString();
		
		try {
			if(!testResult)
			{
				AddServerFactory addServerPage = new AddServerFactory(driver);
				addServerPage.connectToServer(serverName);
				LoginFactory loginPage = addServerPage.navigateToLoginPage();
				loginPage.loginToMfiles(userName, password);
				Vault vault = loginPage.navigateToVaultPage();
				vault.selectVault(testVault);
			}
		
			VaultHome vaultHome = new VaultHome(driver);
			Log.message("Step-1: Started with Vault Home Screen", driver, extentedReport);
			
			vaultHome.clickCreateNewObject();
			Log.message("Step-2: Clicks on create object button", driver, extentedReport);
			
			vaultHome.selectObjectType(objectType);
			vaultHome.selectClassName(className);
			vaultHome.selectTemplate(templateName);
			Log.message("Step-3: Selecting object Type, class and template if any", driver, extentedReport);
			
			ObjectCreationFactory objectCreation = vaultHome.navigateToObjectCreationScreen();
			if(!objectCreation.pageLoaded())
				Log.fail("Object Creation Screen not loaded", driver, extentedReport);
			Log.pass("Step-4: Navigated to Object Creation Screen Successfully!!", driver, extentedReport);
			
			Log.testCaseResult(extentedReport);
		}
		catch (Exception e) {
			Log.exception(e, driver, extentedReport);
		} //End catch
		
		finally {
			Log.endTestCase(extentedReport);
		} //End finally
		
	} //End SmokeCaseTest1_4
	
	/**
	 * TC_005 : Populating properties
	 */
	@Test(description = "Populating properties")
	public void SmokeCaseTest1_5() throws Exception {
		
		ExtentTest extentedReport = Log.testCaseInfo("MFMATest005", "Populating properties", "MFMA_SmokeTest", deviceName);
		
		HashMap <String, String> testData = DataUtils.testDatabyID("MFMATest009", "SmokeTestCases_POC");
		pName = testData.get("pName").toString();
		pAssignment = testData.get("pAssignment").toString();
		pAssigned = testData.get("pAssigned").toString();
		pDeadline = testData.get("pDeadline").toString();
		
		try {
				if(!testResult)
				{
					AddServerFactory addServerPage = new AddServerFactory(driver);
					addServerPage.connectToServer(serverName);
					LoginFactory loginPage = addServerPage.navigateToLoginPage();
					loginPage.loginToMfiles(userName, password);
					Vault vault = loginPage.navigateToVaultPage();
					vault.selectVault(testVault);
					VaultHome vaultHome = vault.navigateToVaultHomeScreen();
					vaultHome.clickCreateNewObject();
					vaultHome.selectObjectType(objectType);
					vaultHome.selectClassName(className);
					vaultHome.selectTemplate(templateName);
				}
		
				ObjectCreationFactory objectCreation = new ObjectCreationFactory(driver);
				Log.message("Step-1: Started with Object Creation Screen", driver, extentedReport);
				
				String objName = "Test_" + Utils.getCurrentDateTime();
				
				LinkedHashMap<String, String> propertyList = new LinkedHashMap<String, String>();
				propertyList.put("Name or title", objName);
				propertyList.put("Assignment description", pAssignment);
				propertyList.put("Assigned to", pAssigned);
				propertyList.put("Deadline", pDeadline);
			
			objectCreation.populatingValuesWithoutType(propertyList);
			Log.message("Step-2: Populated values", driver, extentedReport);
			
			objectCreation.tapOnSaveButton();
			Log.pass("Step-3: Tapped on Save Button", driver, extentedReport);
			
			VaultHome vaultHome = objectCreation.tapOnBackButton();
			Log.message("Step-4: Navigated to VaultHome Screen Successfully", driver, extentedReport);
			
			if(MobileDriverUtils.platform.equalsIgnoreCase("Android"))
				vaultHome.navigateToView(ResourceString.ViewNames.RecentAccessAndroid.Value); //Clicks on the specified view
			else
				vaultHome.navigateToView(ResourceString.ViewNames.RecentAccessIos.Value); //Clicks on the specified view
			Log.message("Step-5: Navigated to Recently Accessed Screen Successfully", driver, extentedReport);
			
			RecentlyAccessedFactory recentlyAccessed = new RecentlyAccessedFactory(driver);	
			if (recentlyAccessed.isItemExists(objName))
				Log.pass("Step 6: Test case Passed. Object (" + objName + ") is created successfully.", driver, extentedReport);
			else
				Log.fail("Test case Failed. Object (" + objName + ") is not created successfully.", driver, extentedReport);
			
			vaultHome = objectCreation.tapOnBackButton();
			Log.message("Step-7: Navigated to VaultHome Screen Successfully", driver, extentedReport);
			
			Log.testCaseResult(extentedReport);
		}
		catch (Exception e) {
			Log.exception(e, driver, extentedReport);
		} //End catch
		
		finally {
			Log.endTestCase(extentedReport);
		} //End finally
		
	} //End SmokeCaseTest1_5
	
	/**
	 * TC_006 : SFD TO MFD Conversion
	 */
	@Test(description = "Populating properties", priority=10)
	public void SmokeCaseTest1_6() throws Exception {
		
		ExtentTest extentedReport = Log.testCaseInfo("MFMATest006", "SFD TO MFD Conversion", "MFMA_SmokeTest", deviceName);
		
		HashMap <String, String> testData = DataUtils.testDatabyID("MFMATest010", "SmokeTestCases_POC");
		if(MobileDriverUtils.platform.equalsIgnoreCase("Android"))
		{
			searchKeyword = testData.get("aSearchKeyword").toString();
			itemName = testData.get("aItemName").toString();
		}
		else
		{
			searchKeyword = testData.get("iSearchKeyword").toString();
			itemName = testData.get("iItemName").toString();
		}
		try {
				if(!testResult)
				{
					AddServerFactory addServerPage = new AddServerFactory(driver);
					addServerPage.connectToServer(serverName);
					LoginFactory loginPage = addServerPage.navigateToLoginPage();
					loginPage.loginToMfiles(userName, password);
					Vault vault = loginPage.navigateToVaultPage();
					vault.selectVault(testVault);
				}
			VaultHome vaultHome = new VaultHome(driver);
			Log.message("Step-1: Started with VaultHome Screen", driver, extentedReport);
			
			vaultHome.navigateToView(ResourceString.ViewNames.Search.Value);
			Log.message("Step-2: Navigated to Search Screen Successfully", driver, extentedReport);
			
			SearchFactory searchFactory = new SearchFactory(driver);
			searchFactory.keywordSearch(searchKeyword);
			searchFactory.selectObject(itemName);
			Log.message("Step 3: Object "+ itemName + " is selected." ,driver, extentedReport);
			
			Thread.sleep(5000);
			ObjectCreationFactory ObjectCreation = new ObjectCreationFactory(driver);
			ObjectCreation.clickMenuItem(ResourceString.MenuItems.SFDToMFD.Value);
			Thread.sleep(5000);
			Log.message("Step 4:" + ResourceString.MenuItems.SFDToMFD.Value + " is selected for object (" + itemName + ")." , driver, extentedReport);
			
			if (ObjectCreation.getAddedFilesCount() >= 1)
				Log.pass("Step 5: Test case Passed. SFD is Converted to MFD", driver, extentedReport);
			else
				Log.fail("Step 5: Test case Failed. SFD is not converted to MFD.", driver, extentedReport);
			
			ObjectCreation.clickMenuItem(ResourceString.MenuItems.MFDToSFD.Value);
			Log.message("Step 6:" + ResourceString.MenuItems.MFDToSFD.Value + " is selected for object (" + itemName + ")." , driver, extentedReport);
			
			Log.testCaseResult(extentedReport);
		}
		catch (Exception e) {
			Log.exception(e, driver, extentedReport);
		} //End catch
		
		finally {
			Log.endTestCase(extentedReport);
		} //End finally
		
	} //End SmokeCaseTest1_6
	
	/**
	 * TC_007 : E-Signature to workflows
	 */
	@Test(description = "E-Signature to workflows", priority=11)
	public void SmokeCaseTest1_7() throws Exception {
		
		ExtentTest extentedReport = Log.testCaseInfo("MFMATest007", "E-Signature to workflows", "MFMA_SmokeTest", deviceName);
		
		HashMap <String, String> testData = DataUtils.testDatabyID("MFMATest011", "SmokeTestCases_POC");
		workflowName = testData.get("workflowName").toString();
		workflowState = testData.get("workflowState").toString();
		
		try {
				if(!testResult)
				{
					AddServerFactory addServerPage = new AddServerFactory(driver);
					addServerPage.connectToServer(serverName);
					LoginFactory loginPage = addServerPage.navigateToLoginPage();
					loginPage.loginToMfiles(userName, password);
					Vault vault = loginPage.navigateToVaultPage();
					vault.selectVault(testVault);
					VaultHome vaultHome = vault.navigateToVaultHomeScreen();
					vaultHome.navigateToView(ResourceString.ViewNames.Search.Value);
					SearchFactory searchFactory = new SearchFactory(driver);
					searchFactory.keywordSearch(searchKeyword);
					searchFactory.selectObject(itemName);
				}
			
			Thread.sleep(5000);
			ObjectCreationFactory objectCreation = new ObjectCreationFactory(driver);
			Log.message("Step-1: Started with ObjectCreation Screen", driver, extentedReport);
			
			objectCreation.switchToPageForward(ResourceString.PropertyPages.Workflow.Value);
			Thread.sleep(5000);
			Log.message("Step 2: Navigated to "+ResourceString.PropertyPages.Workflow.Value+" page of selected object (" + itemName + ")." , extentedReport);
			
			WorkflowsFactory workflowPage = new WorkflowsFactory(driver);
			workflowPage.setWorkflow(workflowName);
			Thread.sleep(5000);
			workflowPage.SaveWorkflow();
			Log.message("Step 3: Workflow type (" + workflowName + ") is selected." , driver, extentedReport);
			
			workflowPage.setWorkflowState(workflowState);
			Thread.sleep(5000);
			workflowPage.SaveWorkflow();
			Log.message("Step 4: Workflow State (" + workflowState + ") is selected." , driver, extentedReport);

			workflowPage.eSignEnterPassword("test");
			Thread.sleep(5000);
			workflowPage.eSignClickSave();
			Log.message("Step 5: Entered the password of the user (" + userName + ")." , driver, extentedReport);
			
			if (workflowPage.getErrorInfo().equalsIgnoreCase(" "))
				Log.pass("Step 6: Test case Passed. Approved electronic signature as (" + userName + ")." , driver, extentedReport);
			else
				Log.fail("Test case Failed. Electronic signature is not successfully completed." , driver, extentedReport);
			Log.testCaseResult(extentedReport);
		}
		catch (Exception e) {
			Log.exception(e, driver, extentedReport);
		} //End catch
		
		finally {
			Log.endTestCase(extentedReport);
		} //End finally
		
	} //End SmokeCaseTest1_7
	
	/**
	 * tearDown : After Class method to perform final operations.
	 */
	@AfterClass
	public void tearDown() throws Exception
	{
		driver.quit();
	}
		 
}
