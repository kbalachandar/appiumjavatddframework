package MFMA.Factories;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;

import MFMA.Screens.RecentlyAccessedScreen;
import genericLibrary.Log;
import genericLibrary.MobileDriverUtils;
import genericLibrary.StopWatch;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class RecentlyAccessedFactory extends RecentlyAccessedScreen
{
	RemoteWebDriver driver;
	
	/**
	 * ListViewPage : ListViewPage constructor
	 * @param driver : RemoteWebdriver
	 * @throws Exception 
	 */
	public RecentlyAccessedFactory(RemoteWebDriver driver) throws Exception {
		
		final long startTime = StopWatch.startTime();
		
		try {
			this.driver = driver;
			PageFactory.initElements(new AppiumFieldDecorator(driver), this);
        }
        catch (Exception e) {
        	throw new Exception("Exception at RecentlyAccessedFactory constructor : " + e);
        }
        finally {
        	Log.event("RecentlyAccessedFactory Constructor", StopWatch.elapsedTime(startTime));
        }
    } //End SearchView
	
	final public void isLoaded(){
    	
	}//isLoaded
	
	final protected void load(){
		
	}//load
	
	/**
	 * clickItem: This method is to perform simple search
	 * @param itemName Name of the item
	 * @return None
	 * @throws Exception
	 */
	public int listSize() throws Exception {
    	
		final long startTime = StopWatch.startTime();
		
		try {
			
			return titleObjectsName.size();
			
		} // End try
		catch (Exception e) {
			throw new Exception("Exception at RecentlyAccessedFactory.listSize : ", e);
		} // End catch

		finally {
			Log.event("RecentlyAccessedFactory.listSize :", StopWatch.elapsedTime(startTime));
		}
		
    } //End clickItem

	/**
	 * isItemExists: This method is check if item exists in the list
	 * @param itemName Name of the item
	 * @return None
	 * @throws Exception
	 */
	public Boolean isItemExists(String itemName) throws Exception {
    	
		final long startTime = StopWatch.startTime();
		
		try {
			if(MobileDriverUtils.platform.equalsIgnoreCase("Android"))
    		{
				int listSize = this.listSize();
				int loopIdx=0;
			
				for (loopIdx=0; loopIdx<listSize; loopIdx++) 
					if (titleObjectsName.get(loopIdx).getText().equalsIgnoreCase(itemName)) 
						return true;
				return false;
    		}
			else
			{
				try
				{
					driver.findElement(By.xpath("//UIATableCell[@name='" + itemName + "']"));
					return true;
				}
				catch(Exception e)
				{
					return false;
				}
			}
	    	
		} // End try
		catch (Exception e) {
			throw new Exception("Exception at RecentlyAccessedFactory.clickItem : ", e);
		} // End catch

		finally {
			Log.event("RecentlyAccessedFactory.clickItem :", StopWatch.elapsedTime(startTime));
		}
		
    } //End clickItem
	
	/**
	 * clickItem: This method is to perform simple search
	 * @param itemName Name of the item
	 * @return None
	 * @throws Exception
	 */
	public ObjectCreationFactory clickItem(String itemName) throws Exception {
    	
		final long startTime = StopWatch.startTime();
		
		try {
			
			int listSize = this.listSize();
			int loopIdx=0;
			
			for (loopIdx=0; loopIdx<listSize; loopIdx++) {
				if (titleObjectsName.get(loopIdx).getText().equalsIgnoreCase(itemName)) {
					titleObjectsName.get(loopIdx).click();
					break;
				}
			}
			
			if (loopIdx >= listSize)
				throw new Exception("Exception at RecentlyAccessedFactory.clickItem : Item (" + itemName + ") does not exists.");
			
			return new ObjectCreationFactory(driver);
	    	
		} // End try
		catch (Exception e) {
			throw new Exception("Exception at RecentlyAccessedFactory.clickItem : ", e);
		} // End catch

		finally {
			Log.event("RecentlyAccessedFactory.clickItem :", StopWatch.elapsedTime(startTime));
		}
		
    } //End clickItem
	
	public VaultHome tapOnBackButton() throws Exception
	{
		btnMenu.click();
		return new VaultHome(driver);
	}
}
